﻿# EP2 - OO 2019.1 (UnB - Gama)

## Funcionamento

 - Para o programa abrir, deve-se executar a classe TelaPrincipal;
 - Quando o programa abrir, a primeira aba (Início) é utilizada somente para salvar e carregar o sistema quando fechar e iniciar o programa, respectivamente;
 - Na segunda aba (Veículos) é possível cadastrar e descadastrar veículos na empresa. Também nessa aba, é possível retornar um veículo de uma entrega que estava sendo feita. Ainda é possível verificar a quantidade de veículos totais na empresa, a quantidade de veículos disponíveis para a entrega e a quantidade de veículos que está em rota de entrega;
 - Na terceira aba (Dados/Lucro) é possível verificar as informações de cada veículo e atualizar o lucro da empresa e os preços dos combustíveis;
 - Na quarta aba (Entregas) é onde é feita o cálculo do frete, onde é dado o feedback se é possível fazer a entrega e a escolha do veículo, se possível. O valor do frete no feedback já está somado com o lucro da empresa;

## Programas utilizados

 - Foi utilizado o eclipse versão 2018/09;
 - Para a elaboração do Diagrama de Classes, foi utilizado o site draw.io. Não foi possível gerar uma imagem, logo para verificar o Diagrama, basta entra no site https://www.draw.io/ e colocar para carregar um arquivo.

## Persistência

 - Para a persistência, foi utilizado a serialização da classe Sistema, salvando o mesmo em um arquivo Sistema.dat (arquivo binário).
                                                                                                                                                      
