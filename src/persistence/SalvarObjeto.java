package persistence;

import java.io.FileOutputStream;
import java.io.ObjectOutputStream;

import application.Sistema;

public class SalvarObjeto {
	
	public void salvarSistema(Sistema sistemaParaSalvar) {
		
		try {
			FileOutputStream saveFile = new FileOutputStream("../ep2/src/persistence/Sistema.dat");
			ObjectOutputStream stream = new ObjectOutputStream(saveFile);
			
			stream.writeObject(sistemaParaSalvar);
			stream.flush();
			stream.close();
			saveFile.flush();
			saveFile.close();
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
}
