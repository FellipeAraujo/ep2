package tests;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import application.Sistema;
import vehicles.Veiculo;

class SistemaTest {
	public List<Veiculo> listaCarretaDisponivel;
	public List<Veiculo> listaCarretaEmRota;
	public List<Veiculo> listaVanDisponivel;
	public List<Veiculo> listaVanEmRota;
	public List<Veiculo> listaCarroDisponivel;
	public List<Veiculo> listaCarroEmRota;
	public List<Veiculo> listaMotoDisponivel;
	public List<Veiculo> listaMotoEmRota;
	public double lucro;
	public double precoDiesel;
	public double precoGasolina;
	public double precoAlcool;
	public Sistema sistema;
	
	@BeforeEach
	public void beforeTests() {
		// Prepara��o
		listaCarretaDisponivel = new ArrayList<>();
		listaCarretaEmRota = new ArrayList<>();
		listaVanDisponivel = new ArrayList<>();
		listaVanEmRota = new ArrayList<>();
		listaCarroDisponivel = new ArrayList<>();
		listaCarroEmRota = new ArrayList<>();
		listaMotoDisponivel = new ArrayList<>();
		listaMotoEmRota = new ArrayList<>();
		lucro = 20.0;
		precoDiesel = 3.869;
		precoGasolina = 4.449;
		precoAlcool = 3.499;
		sistema = new Sistema();
	}

	@Test
	public void testaConstrutor() {
		// A��o
		sistema = new Sistema();
		
		// Verifica��o
		assertEquals(listaCarretaDisponivel, sistema.getListaCarretaDisponivel());
		assertEquals(listaCarretaEmRota, sistema.getListaCarretaEmRota());
		assertEquals(listaVanDisponivel, sistema.getListaVanDisponivel());
		assertEquals(listaVanEmRota, sistema.getListaVanEmRota());
		assertEquals(listaCarroDisponivel, sistema.getListaCarroDisponivel());
		assertEquals(listaCarroEmRota, sistema.getListaCarroEmRota());
		assertEquals(listaMotoDisponivel, sistema.getListaMotoDisponivel());
		assertEquals(listaMotoEmRota, sistema.getListaMotoEmRota());
		assertEquals(lucro, sistema.getLucro());
		assertEquals(precoDiesel, sistema.getPrecoDiesel());
		assertEquals(precoGasolina, sistema.getPrecoGasolina());
		assertEquals(precoAlcool, sistema.getPrecoAlcool());
	}
	
	@Test
	public void testaCalculaTempoMaximo() {
		double distancia = 120.0;
		double velocidadeMedia = 60.0;
		String tipoVeiculo = "Carreta";
		
		double tempoMaximo = 2.0;
		
		assertEquals(tempoMaximo, sistema.calculaTempoMaximo(distancia, velocidadeMedia, tipoVeiculo));
	}
	
	@Test
	public void testaCalculaCusto() {
		double distancia = 120.0;
		double rendimento = 14.0;
		String tipoVeiculo = "carro";
		int tipoCombustivel = 1;
		
		double custo = 38.1342857;
		
		assertEquals(custo, sistema.calculaCusto(distancia, rendimento, tipoVeiculo, tipoCombustivel));
	}
	
	@Test
	public void testaCalculaCustoBeneficio() {
		double custo = 50.0;
		double tempo = 20.0;
		String tipoVeiculo = "Carreta";
		
		double custoBeneficio = 2.5;
		
		assertEquals(custoBeneficio, sistema.calculaCustoBeneficio(custo, tempo, tipoVeiculo));
	}

	@AfterEach
	void afterTests() {
		sistema = null;
	}
}
